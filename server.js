const http = require('http');
const express = require('express');
var bodyParser = require('body-parser');

const controller = require('./app/eventController');
const app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.json());
app.use('/event', controller);
app.use('/', function(req, res) {
    res.send('event api live');
});
const server = http.createServer(app);
const port = 8888;
server.listen(port);
console.debug('Server listening on port ' + port);