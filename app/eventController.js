const express = require('express');
const router = express.Router();
const organizerService = require('./organizerService');
const userService = require('./userService.js');
const data = require('./dao')
const elm = require('./eventListingManager');
const e = require('express');

router.post('/', function (req, res) {

    let o = organizerService.getOrganizer(userService.getUserId());

    if (req.body.title == null) { res.status(400); return; }
    if (req.body.description == null) { res.status(400); return; }
  
    let e = {
        title: req.body.title,
        description: req.body.description,
        organizer: o.name,
      };
    
    if (req.body.starts != null) {
        const starts = Date.parse(req.body.start);
    }

    if (req.body.end != null) {
        const ends = Date.parse(req.body.end);
    }

    if (starts > ends) {
        res.status(400);
        return;
    }

    e.capacity = req.body.capacity;
    e.tickets = req.body.tickets;

    if (req.body.state = 'published') {
        e.state = 'published';

        if(starts <= Date.now() || e.capacity <= 0 || e.tickets.capacity != capacity) {
            res.status(400); return;
        }

        let r = calc(e);
        e.potentialRevenue=r;

        elm.notify(e);
    } else {
        e.state = 'draft';
    }

    data.save(e);
  
    res.status(201).json(newItem);
  });

  calc = function(x) {
      return e.tickets.capacity * e.tickets.cost;
  }

  module.exports = router;